Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2010-2024, Red Hat, Inc.
 2010-2024, Milan Broz <gmazyland@gmail.com>
License: LGPL-2.1

Files: lib/crypto_backend/argon2/blake2/*
Copyright: © 2015 Daniel Dinu
 © 2015 Dmitry Khovratovich
 © 2015 Jean-Philippe Aumasson
 © 2015 Samuel Neves
License: CC0 or Apache-2.0

Files: misc/keyslot_checker/*
Copyright: © 2004      Christophe Saout <christophe@saout.de>
 © 2004-2008 Clemens Fruhwirth <clemens@endorphin.org>
 © 2008-2023 Red Hat, Inc.
 © 2008-2023 Milan Broz <gmazyland@gmail.com>
License: GPL-2+ with OpenSSL exception

Files: FAQ.md
Copyright: no-info-found
License: JSON

Files: debian/*
Copyright: 2016-2023, Guilhem Moulin <guilhem@debian.org>
 2006-2008, David Härdeman <david@hardeman.nu>
 2005-2015, Jonas Meurer <jonas@freesources.org>
 2005, 2006, Michael Gebetsroither <michael.geb@gmx.at>
 2004, 2005, Wesley W. Terpstra <terpstra@debian.org>
License: GPL-2+

Files: debian/bash_completion/*
Copyright: 2013, Claudius Hubig <cl_crds@chubig.net>, 2-clause BSD
License: GPL-2+

Files: debian/cryptsetup.apport
Copyright: 2015, Author: Jonas Meurer <jonas@freesources.org>
 2009, Author: Reinhard Tartler <siretart@tauware.de>
License: GPL-2+

Files: debian/initramfs-gplv2/*
Copyright: 2015-2018, Guilhem Moulin <guilhem@debian.org>
License: GPL-2+

Files: debian/initramfs/cryptroot-unlock
Copyright: 2015-2018, 2021, 2022, Guilhem Moulin <guilhem@debian.org>
License: GPL-3+

Files: debian/scripts/cryptdisks_start
Copyright: 2007, Jon Dowland <jon@alcopop.org>
License: GPL-2+

Files: debian/scripts/luksformat
Copyright: 2005, Canonical Ltd.
License: GPL-2+

Files: debian/scripts/suspend/cryptsetup-suspend-wrapper
Copyright: 2020-2022, Guilhem Moulin <guilhem@debian.org>
 2019, 2020, Tim <tim@systemli.org>
 2019, 2020, Jonas Meurer <jonas@freesources.org>
License: GPL-2+

Files: debian/scripts/suspend/cryptsetup-suspend.c
Copyright: 2018-2020, Jonas Meurer <jonas@freesources.org>
 2018, Guilhem Moulin <guilhem@debian.org>
License: GPL-3+

Files: debian/tests/cryptroot-run
Copyright: 2015-2018, 2021, 2022, Guilhem Moulin <guilhem@debian.org>
License: GPL-3+

Files: debian/tests/utils/*
Copyright: 2015-2018, 2021, 2022, Guilhem Moulin <guilhem@debian.org>
License: GPL-3+

Files: debian/tests/utils/initramfs-hook.common
Copyright: 2016-2023, Guilhem Moulin <guilhem@debian.org>
 2006-2008, David Härdeman <david@hardeman.nu>
 2005-2015, Jonas Meurer <jonas@freesources.org>
 2005, 2006, Michael Gebetsroither <michael.geb@gmx.at>
 2004, 2005, Wesley W. Terpstra <terpstra@debian.org>
License: GPL-2+

Files: docs/v1.6.0-ReleaseNotes
Copyright: no-info-found
License: GPL-2

Files: docs/v2.3.0-ReleaseNotes
Copyright: no-info-found
License: RPSL

Files: lib/bitlk/*
Copyright: 2019-2024, Vojtech Trefny
 2019-2024, Red Hat, Inc.
 2019-2024, Milan Broz <gmazyland@gmail.com>
License: LGPL-2.1

Files: lib/bitops.h
Copyright: no-info-found
License: public-domain

Files: lib/crypt_plain.c
Copyright: 2010-2024, Red Hat, Inc.
 2010-2024, Milan Broz <gmazyland@gmail.com>
 2004, Jana Saout <jana@saout.de>
License: GPL-2

Files: lib/crypto_backend/argon2/*
Copyright: no-info-found
License: CC0-1.0

Files: lib/crypto_backend/argon2/encoding.c
Copyright: 2015, Daniel Dinu, Dmitry Khovratovich, Jean-Philippe Aumasson, and Samuel Neves
License: CC0-1.0

Files: lib/crypto_backend/base64.c
Copyright: 2021-2024, Milan Broz <gmazyland@gmail.com>
 2010, Lennart Poettering
License: LGPL-2.1

Files: lib/crypto_backend/pbkdf2_generic.c
Copyright: 2012-2024, Red Hat, Inc.
 2012-2024, Milan Broz <gmazyland@gmail.com>
 2004, Free Software Foundation
 2002, 2003, Simon Josefsson
License: LGPL-2.1

Files: lib/crypto_backend/pbkdf_check.c
Copyright: 2016-2020, Ondrej Mosnacek
 2012-2024, Red Hat, Inc.
 2012-2024, Milan Broz <gmazyland@gmail.com>
License: LGPL-2.1

Files: lib/crypto_backend/utf8.c
Copyright: 2021-2024, Vojtech Trefny
 2010, Lennart Poettering
License: LGPL-2.1

Files: lib/fvault2/*
Copyright: 2021, 2022, Pavel Tobias
License: LGPL-2.1

Files: lib/internal.h
 lib/libcryptsetup.h
 lib/libdevmapper.c
 lib/setup.c
 lib/utils.c
 lib/utils_device.c
 lib/utils_devpath.c
 lib/utils_dm.h
 lib/utils_io.c
 lib/utils_io.h
Copyright: 2009-2024, Red Hat, Inc.
 2009-2024, Milan Broz <gmazyland@gmail.com>
 2004-2007, Clemens Fruhwirth <clemens@endorphin.org>
 2004, Jana Saout <jana@saout.de>
License: GPL-2

Files: lib/keyslot_context.c
 lib/keyslot_context.h
 lib/utils_device_locking.c
 lib/utils_device_locking.h
 lib/utils_keyring.c
 lib/utils_keyring.h
Copyright: 2015-2024, Red Hat, Inc.
 2015-2024, Ondrej Kozina
License: GPL-2

Files: lib/libcryptsetup_macros.h
 lib/utils_benchmark.c
 lib/utils_loop.c
 lib/utils_loop.h
 lib/utils_pbkdf.c
 lib/utils_safe_memory.c
Copyright: 2009-2024, Red Hat, Inc.
 2009-2024, Milan Broz <gmazyland@gmail.com>
License: GPL-2

Files: lib/libcryptsetup_symver.h
 lib/random.c
 lib/utils_blkid.c
 lib/utils_blkid.h
Copyright: 2010-2024, Red Hat, Inc.
License: GPL-2

Files: lib/luks1/*
Copyright: 2009-2024, Red Hat, Inc.
 2004-2006, Clemens Fruhwirth <clemens@endorphin.org>
License: GPL-2

Files: lib/luks1/keyencryption.c
 lib/luks1/keymanage.c
Copyright: 2009-2024, Red Hat, Inc.
 2009-2024, Milan Broz <gmazyland@gmail.com>
 2004-2007, Clemens Fruhwirth <clemens@endorphin.org>
License: GPL-2

Files: lib/luks2/*
Copyright: 2009-2024, Red Hat, Inc.
 2009-2024, Milan Broz <gmazyland@gmail.com>
License: GPL-2

Files: lib/luks2/hw_opal/*
Copyright: 2022, 2023, Luca Boccassi <bluca@debian.org>
License: LGPL-2.1

Files: lib/luks2/luks2_json_metadata.c
 lib/luks2/luks2_luks1_convert.c
 lib/luks2/luks2_reencrypt_digest.c
Copyright: 2015-2024, Ondrej Kozina
 2009-2024, Red Hat, Inc.
 2009-2024, Milan Broz <gmazyland@gmail.com>
License: GPL-2

Files: lib/luks2/luks2_keyslot_reenc.c
 lib/luks2/luks2_reencrypt.c
 lib/luks2/luks2_segment.c
 lib/luks2/luks2_token_keyring.c
Copyright: 2015-2024, Red Hat, Inc.
 2015-2024, Ondrej Kozina
License: GPL-2

Files: lib/utils_crypt.c
 lib/utils_crypt.h
 lib/utils_wipe.c
Copyright: 2009-2024, Red Hat, Inc.
 2009-2024, Milan Broz <gmazyland@gmail.com>
 2004-2007, Clemens Fruhwirth <clemens@endorphin.org>
License: GPL-2

Files: lib/utils_storage_wrappers.c
 lib/utils_storage_wrappers.h
Copyright: 2018-2024, Ondrej Kozina
License: LGPL-2.1

Files: lib/verity/rs.h
 lib/verity/rs_decode_char.c
 lib/verity/rs_encode_char.c
Copyright: 2017-2024, Red Hat, Inc.
 2002, 2004, Phil Karn, KA9Q
License: LGPL-2.1

Files: lib/verity/verity_fec.c
Copyright: 2017-2024, Red Hat, Inc.
 2015, Google, Inc.
License: LGPL-2.1

Files: lib/volumekey.c
Copyright: 2009-2024, Red Hat, Inc.
 2004-2006, Clemens Fruhwirth <clemens@endorphin.org>
License: GPL-2

Files: m4/*
Copyright: 2011, Maarten Bosmans <mkbosmans@gmail.com>
 2008, Guido U. Draheim <guidod@gmx.de>
License: FSFAP

Files: misc/dict_search/crypt_dict.c
Copyright: 2012, 2018-2024, Milan Broz <gmazyland@gmail.com>
License: GPL-2

Files: misc/fedora/*
Copyright: no-info-found
License: GPL-2

Files: misc/keyslot_checker/chk_luks_keyslots.c
Copyright: 2012, Arno Wagner <arno@wagner.name>
License: GPL-2

Files: misc/luks-header-from-active
Copyright: 2010-2012, Milan Broz <gmazyland@gmail.com>
License: LGPL-2.1+

Files: src/*
Copyright: 2015-2024, Red Hat, Inc.
 2015-2024, Ondrej Kozina
License: GPL-2

Files: src/cryptsetup.c
 src/cryptsetup.h
 src/utils_tools.c
Copyright: 2009-2024, Red Hat, Inc.
 2009-2024, Milan Broz <gmazyland@gmail.com>
 2004-2007, Clemens Fruhwirth <clemens@endorphin.org>
 2004, Jana Saout <jana@saout.de>
License: GPL-2

Files: src/integritysetup.c
 src/utils_password.c
 src/utils_progress.c
 src/utils_reencrypt_luks1.c
 src/veritysetup.c
Copyright: 2009-2024, Red Hat, Inc.
 2009-2024, Milan Broz <gmazyland@gmail.com>
License: GPL-2

Files: src/utils_luks.c
 src/utils_luks.h
 src/utils_reencrypt.c
Copyright: 2015-2024, Ondrej Kozina
 2009-2024, Red Hat, Inc.
 2009-2024, Milan Broz <gmazyland@gmail.com>
License: GPL-2

Files: tests/api-test-2.c
 tests/api-test.c
 tests/api_test.h
Copyright: 2015-2024, Ondrej Kozina
 2009-2024, Red Hat, Inc.
 2009-2024, Milan Broz <gmazyland@gmail.com>
License: GPL-2

Files: tests/crypto-vectors.c
 tests/unit-utils-crypt.c
 tests/unit-wipe.c
Copyright: 2012, 2018-2024, Milan Broz <gmazyland@gmail.com>
License: GPL-2

Files: tests/differ.c
 tests/unit-utils-io.c
Copyright: 2010-2024, Red Hat, Inc.
License: GPL-2

Files: tests/fake_systemd_tpm_path.c
Copyright: no-info-found
License: GPL-2

Files: tests/fuzz/*
Copyright: 2022-2024, Red Hat, Inc.
 2022-2024, Daniel Zatovic <daniel.zatovic@gmail.com>
License: GPL-2

Files: tests/fuzz/FuzzerInterface.h
Copyright: no-info-found
License: Apache-2.0

Files: tests/fuzz/json_proto_converter.cc
 tests/fuzz/json_proto_converter.h
Copyright: 2020, Google Inc.
License: Apache-2.0

Files: tests/test_utils.c
Copyright: 2009-2024, Red Hat, Inc.
 2009-2024, Milan Broz <gmazyland@gmail.com>
License: GPL-2

Files: tokens/ssh/*
Copyright: 2020-2024, Vojtech Trefny
 2016-2024, Milan Broz <gmazyland@gmail.com>
License: LGPL-2.1

Files: tokens/ssh/cryptsetup-ssh.c
Copyright: 2021-2024, Vojtech Trefny
 2016-2024, Milan Broz <gmazyland@gmail.com>
License: GPL-2

Files: lib/crypto_backend/argon2/argon2.c lib/crypto_backend/argon2/argon2.h lib/crypto_backend/argon2/core.c lib/crypto_backend/argon2/core.h lib/crypto_backend/argon2/encoding.h lib/crypto_backend/argon2/opt.c lib/crypto_backend/argon2/ref.c lib/crypto_backend/argon2/thread.c lib/crypto_backend/argon2/thread.h
Copyright: © 2015 Daniel Dinu
 © 2015 Dmitry Khovratovich
 © 2015 Jean-Philippe Aumasson
 © 2015 Samuel Neves
License: CC0 or Apache-2.0

Files: lib/crypto_backend/crc32.c
Copyright: © 1986 Gary S. Brown
License: public-domain
 Gary S. Brown's license is as follows:
 .
 You may use this program, or code or tables extracted from it, as
 desired without restriction.
